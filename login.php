<!DOCTYPE html>
<html>
<head>
  <title> Life | Login </title>
  <?php

    include('data/config.php');

  ?>
  <link href='<?php echo $startup; ?>' rel='stylesheet' type='text/css'>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>

  <script>
  $(function(){

    $(".done").click(function(){

  var user = $("#username").val();
  var pass = $("#password").val();

  if(pass === '' || user === ''){

    alert("Username or password cannot be empty!");
	$("#username").focus();
	return false;
  }
});
});
</script>
</head>
<body>

  <div class="login" style="font-family: 'Lato', sans-serif;">

    <h1> Login </h1>

    <div style="margin: 0 auto; text-align: center;">

    <form action="panel.php" method="POST">
      <input type="text" name="username" id="username" class="text" placeholder="Username">
      <br><br>
      <input type="password" name="password" id="password" class="text" placeholder="Password">
      <br><br>
      <button class="done" style="width: 200px;" value="Login">Login</button>
    </form>

    <?php
    if(!file_exists('data/config.json')){

      echo "<p style='font-size: 1rem; left: 0; right: 0; position: absolute; color: white; text-decoration: none;'>Config file does not exists! Consider to <a style='color: white;' href='startup.php'>setup your blog</a> first.</p>";

    }else{

      echo "<p style='font-size: 1rem; left: 0; right: 0; position: absolute; color: white; text-decoration: none;'><a style='color: white;' href='forgot.php'>I forgot my password</a></p>";
    }
?>
  </div>

  </div>

</body>

</html>
