<!DOCTYPE html>
<html>

<head>
  <?php

    include('data/config.php');

  ?>
  <title> Life | Configuration </title>
  <link href='css/startup.css' rel='stylesheet' type='text/css'>

</head>

  <body>
    <div class="life">

      Configure Life

      <h3> All fields are required </h3>
<?php
      if(file_exists($config)){

        header("Location: login.php");

      }else{

        echo "<h3 style='text-decoration: none;'>It seems that this is the first time we meet!</h3>";

      }
?>
    </div>

    <div class="config">

    <form action="done.php" method="POST">

      Tell me your name <input class="text" type="text" name="name" placeholder="Andrew Ryan">
      <br><br>
      Blog's name? <input class="text" type="text" name="blog_name" placeholder="Under The Sea">
      <br><br>
      Insert your username <input class="text" type="text" name="username" placeholder="andrewryan">
      <br><br>
      Insert your password <input class="text" type="password" name="password" placeholder="Password here">
      <br><br>
      Insert your E-mail address <input class="text" type="text" name="mail" placeholder="E-Mail here">
      <br><br>
      Tell me about you<br>
      <textarea cols="50" rows="5" name="infos" placeholder="I chose the impossible, I chose Rapture..."></textarea>
      <br><br>
      <button value="Done" class="done">Done</button>
    </form>

    </div>

  </body>

</html>
