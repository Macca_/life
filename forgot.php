<!DOCTYPE html>
<html>
<head>
  <title> Life | Recover </title>
  <link href='css/startup.css' rel='stylesheet' type='text/css'>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>

  <script>
  $(function(){

    $(".done").click(function(){

  var mail = $("#mail").val();

  if(mail === ''){

    alert("Please, insert an e-mail address");
	$("#mail").focus();
	return false;
  }
});
});
</script>
</head>
<body>

  <div class="login" style="font-family: 'Lato', sans-serif; height: 300px; width: 300px;">

    <h1> Password recovery </h1>

    <div style="margin: 0 auto; text-align: center;">

    <form action="forgot.php" method="POST">
      <input type="text" name="mail" id="mail" class="text" placeholder="The e-mail you registered">
      <br><br>
      <button class="done" style="width: 200px;" value="Confirm">Confirm</button>
    </form>

    <?php

  $newp = mt_rand(0, 100000);


    $config = 'data/config.json';

    $content = file_get_contents($config);
    $data = json_decode($content, true);
    $mail = $data[0]["mail"];

    if(!file_exists($config)){

      echo "<p style='font-size: 1rem; left: 0; right: 0; position: absolute; color: white; text-decoration: none;'>You didn't setup your blog, so you can't recover a password that doesn't exists.</p>";

    }else{

    if(isset($_POST["mail"]) && $_POST["mail"] == $mail){

      $getConfig = file_get_contents($config);
      $configDecoded = json_decode($getConfig, true);
      $configDecoded[0]['password'] = $newp;
      $encode = json_encode($configDecoded);
      file_put_contents($config, $encode);

      mail($mail, "Life: Your blog's password has been changed!", "Hello there!\n Your new password is -> $newp\nChange it as soon as you can, in the Control Panel.");

      echo "<p style='font-size: 1rem; left: 0; right: 0; position: absolute; color: white; text-decoration: none;'>A mail with instruction has been sent to $mail, also check your spam folder!</p>";
    }


    if(isset($_POST["mail"]) && $_POST["mail"] != $mail){

      echo "<p style='font-size: 1rem; left: 0; right: 0; position: absolute; color: white; text-decoration: none;'>".$_POST['mail']." is not the one you registered.</p>";


    }
}

?>
  </div>

  </div>

</body>

</html>
