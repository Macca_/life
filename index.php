<?php

  include('data/config.php');

  if(!file_exists($config)){

    header('Location: startup.php'); 

  }

  $content = file_get_contents($config);
  $data = json_decode($content, true);

  $name = $data[0]['name'];
  $blog_name = $data[0]['blog_name'];

  echo $blog_name . " | " . $name . "'s blog - Made with Life Engine";

?>
