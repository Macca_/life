<!DOCTYPE html>
<?php

include('data/config.php');
if(!file_exists($config)){

  header("Location: startup.php");


  }

  $content = file_get_contents($config);
  $data = json_decode($content, true);

    $name = $data[0]['name'];
    $password = $data[0]['password'];
    $username = $data[0]['username'];

    if(!isset($_POST['username'], $_POST['password'])){

      header("Location: login.php");

    }else{

      if($_POST['username'] != $username || $_POST['password'] != $password){

        die("The combination username/password is not correct.");

      }

    }

?>
<html>
  <head>
    <title> Panel | Life </title>
    <link href="<?php echo $style; ?>" rel="stylesheet" type="text/css">

  </head>

  <body>

    <div class="side">

      <div class="welcome">

        <?php echo "Welcome, $name"; ?>

      </div>

        <div class="life">

          Life Web Panel - <div style="display: inline-block;"> Post </div>

        </div>

      <a href="#main">
      <div class="inside">
        Fagiani
      </div>
    </a>
      <div class="inside">
        Peti
      </div>
      <div class="inside">
        Piccioni
      </div>
      <div class="inside">
        Volpi
      </div>
      <div class="inside">
        Compra Google con un click
      </div>
      <div class="inside">
        Suini
      </div>
      <div class="inside">
        Fagianissimi
      </div>

      <div class="post">

        <form action="#" method="POST">

            <input type="text" class="text" name="title" placeholder="Post Title">

            <textarea name="post" cols="100" rows="10" placeholder="Enter post content"></textarea>

        </form>

        <div class="rightside">

          <div class="rightin">

            Opzione 1

          </div>

          <div class="rightin">

            Opzione 2

          </div>
          <div class="rightin">

            Opzione 3

          </div>
          <div class="rightin">

            Opzione 4

          </div>
          <div class="rightin">

            Opzione 5

          </div>
          <div class="rightin">

            Opzione 6

          </div>

          <div class="rightin">

            Opzione 7

          </div>

          <div class="rightin">

            Opzione 8

          </div>

          <div class="rightin">

            Opzione 9

          </div>

        </div>

    </div>

  </body>

</html>
