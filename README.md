# Life ReadMe file #

### What is Life? ###

Life is a blogging platform thought for the single person.
You can use it like a diary, a book, you can write stories, simple thoughts, everything you can imagine.
Life has a simple flat style, that you can obviously customize as you want (by modifying the css source), you can also edit the css of the panel itself, this because Life is you.
Your thoughts, are here.

### How does it work ###

Life is written in PHP, it does NOT require a database, so you can be ready to go as soon as you download it.

### Installation ###

- Via Terminal: `wget https://download.life-engine.com/latest/<life-version-here>`.

- Manual download: every version of Life will be uploaded to https://download.life-engine.com.

When you have downloaded Life, be sure to block access to the `data` folder from the web.

Nginx example:

```
location ~ /path/to/data {
      deny  all;
}

```
Also make sure that Life's folder has the right permissions to write and read from files.
